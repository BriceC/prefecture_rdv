import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Main {
	public static volatile boolean stop = false;

	public static synchronized WebDriver createWebClient(boolean javascript) {
		System.setProperty("webdriver.chrome.driver", "c:\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		return driver;
	}

	public static void main(String[] args) {
		
		String url = "https://www.hauts-de-seine.gouv.fr/booking/create/14086";

		final ExecutorService pool = Executors.newFixedThreadPool(2);
		pool.execute(new Runnable() {
			public void run() {
				while (!stop) {
					final WebDriver webClient = createWebClient(false);
					callSite(url, webClient, "planning14089");
					try {
						Thread.sleep(60000*3);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		pool.execute(new Runnable() {
			public void run() {
				while (!stop) {
					final WebDriver webClient = createWebClient(false);
					callSite(url, webClient, "planning16467");
					try {
						Thread.sleep(60000*3);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

	}

	private static void callSite(String url, final WebDriver webClient, final String string) {
		try {
			processPage(webClient, url, string);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (!stop) {
				try {
					webClient.close();
				} catch (Exception e) {
				}
			}
		}
	}

	public static void waitForLoad(WebDriver driver) throws InterruptedException {
		Thread.sleep(3000);
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	private static void processPage(final WebDriver driver, String url, String string) throws IOException, MalformedURLException, InterruptedException {
		driver.navigate().to(url);
		waitForLoad(driver);
		
		try {
			driver.findElement(By.id("tarteaucitronPersonalize2")).click();
		} catch (Exception e) {
		}
		
		try {
			driver.findElement(By.id("condition")).click();
		} catch (Exception e) {
		}
		driver.findElement(By.name("nextButton")).click();

		waitForLoad(driver);

		driver.findElement(By.id(string)).click();//
		driver.findElement(By.name("nextButton")).click();

		waitForLoad(driver);

		String text = driver.findElement(By.name("create")).getText();

		if (!text.contains("Il n'existe plus de plage horaire libre pour votre demande de rendez-vous. Veuillez recommencer ultérieurement.")) {
			sendMail();
			stop = true;
		}

		Thread.sleep(1000);

	}

	private static void sendMail() {

		Properties prop = new Properties();

		final String username = "from@gmail.com";
		final String password = "pwd";
		prop.put("mail.smtp.host", "smtp.gmail.com");

		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.transport.protocol", "smtps");
		prop.put("mail.smtp.socketFactory.fallback", "false");
		prop.put("mail.smtp.ssl.trust", "*");
		prop.put("mail.smtp.ssl.protocols", "TLSv1.2");

		Session session = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("to@gmail.com"));
			message.setSubject("RDV Prefecture");
			message.setText("Bonjour, vous avez un rdv avec la prefecture...merci de vérifier votre PC.");

			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			e.printStackTrace();
		}

	}
}